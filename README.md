# **心食譜網頁爬蟲**

<p align="center">
    <a href="https://www.python.org/downloads/release/python-368/">
        <img src="https://img.shields.io/badge/python-3.6.8-blue">
    </a>
    <a href="https://pypi.org/project/requests/">
        <img src="https://img.shields.io/badge/requests-2.26.0-blue">
    <a>
    <a href="https://pypi.org/project/beautifulsoup4/">
        <img src="https://img.shields.io/badge/BeautifulSoup4-4.9.3-blue">
    </a>
<p>