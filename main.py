import requests;
import json;
import time;
from bs4 import BeautifulSoup;

data = [];

spider = [
    ['                          :IS1:                          '],
    ['                        :BBQBBBB.                        '],
    ['              .rsL      BBBBBBQBB     .sLi               '],
    ['           .gBBBBBBQi   gQBBBBBBd   7BBBBQBQq            '],
    ['          .BB:    iBBB: bBQBQBBBX iBBB:    iBB           '],
    ['                    .QBBBBBBBBBQBBBB                     '],
    ['             rMBBB1 :BBBBBQBBBBBQBQB.:MBBB5:             '],
    ['          :QBQQrrEBBBBBQBBBBBBBBBQBBBBBjiUBBB1           '],
    ['          EE       vBBBBBBQBBBBBBBBBQ:      rBi          '],
    ['              iBBBBQBBBBBBBBBQBBBBBBBQBBBR.              '],
    ['            .BBMrriigBBBBBBBQBBBBBQBdr7YvBQB             '],
    ['           JBB      7BBBBBBBBBBQBBBBi     .BBi           '],
    ['           BJ   BBBBBBQBBBBBBBBBBBBBBBBBZ   EB           '],
    ['              vQB:      .sDRBQZ7.      iBB:              '],
    ['             sQZ                         BBr             '],
    ['             Bq                           QQ             ']
];

def get_current_web_page(url:str):
    headers = {
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36'
    };
    
    res = requests.get(url, headers=headers);

    if res.status_code == 200:
        print('target url response success', end='\n\n');
        return res.text;
    else:
        print('target url response failed');

def get_target_url_infomation(dom:str):
    soup = BeautifulSoup(dom, 'html.parser');

    recipeName = soup.select('.recipe-exp > .re-up > h1.font18.no-overflow')[0].text;             #食譜名稱
    assess = soup.select('.recipe-exp > .re-up > div > span > .font16.ml10.col > span')[0].text;  #評分
    recipeNumber = soup.select('.recipe-exp > .re-up > .cg2.mt12 > span:nth-child(2)')[0].text;  #食譜號
    readNumber = soup.select('.recipe-exp > .re-up > .cg2.mt12 > span:nth-child(4)')[0].text.replace('次', '');
    
    data.append({
        'recipeName': recipeName,
        'assess': assess,
        'recipeNumber': recipeNumber,
        'readNumber': readNumber
    });

if __name__ == '__main__':
    print('\033[1;32;40m');

    for i in range(0, len(spider)):
        for j in range(0, len(spider[0])):
            print(spider[i][j]);
            time.sleep(0.02);

    res:str = get_current_web_page('https://www.xinshipu.com/zuofa/49391');
    
    if res != '':
        get_target_url_infomation(res);

        if (len(data)):
            print(data);
            
            with open('./data.json', 'w', encoding='utf-8') as f:
                json.dump(data, f, indent=2, sort_keys=False, ensure_ascii=False);